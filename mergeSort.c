#include <stdio.h>
#include <stdlib.h>

void insertion_sort(long int * v, long int m, long int n) {
    long int i, j, aux, x;
    
    for(i =+ m; i < n; i++) {
        x = v[i];
        j = i;

        while(x < v[j - 1] && j > 0) {
            v[j] = v[j - 1];
            j--;
        }

        v[j] = x;
    }
}

void mergeSort(int * a, int inicio, int fim) {
    int meio;

    if(inicio < fim) {
        meio = floor((inicio + fim) / 2);
        mergeSort(a, inicio, meio);
        mergeSort(a, meio + 1, fim);

        if(a[meio] <= a[meio + 1]) return;

        merge(a, inicio, meio, fim);
    }
}

void merge(int * a, int inicio, int meio, int fim) {
    int i, j, k;
    int *aux;

    aux = (int *) malloc(sizeof(int) * fim + 1);

    for(i = meio + 1; i > inicio; i--) aux[i - 1] = a[i - 1];
    for(j = meio; j < fim; j++) aux[fim + meio - j] = a[j + 1];

    for(k = inicio; k <= fim; k++) {
        if(aux[j] < aux[i])
            a[k] = aux[j--];
        else
            a[k] = aux[i++];
    }
}

int main() {
    int N, M, i, j;
    int inicio, meio, fim;
    int cutoff = 5;
    int *a;

    scanf("%d", &N);
    scanf("%d", &M);
    
    a = (int *) malloc(sizeof(int) * (N + M));
    
    for(i = 0; i < N; i++)
        scanf("%d", &a[i]);
    
    for(i = N; i < N + M; i++)
        scanf("%d", &a[i]);

    inicio = 0;
    meio = N - 1;
    fim = N + M - 1;

    merge(a, inicio, meio, fim);

    i = 0;
    meio = cutoff;
    while(meio <= fim) {
        insertion_sort(a, inicio, meio);
        
        inicio = meio;
        meio += cutoff;
        i++;

        if (i % 2 == 0) {
            mergeSort(a, inicio - cutoff, meio);
        }
    }
  
    for(i = 0; i < N + M; i++)
        printf("%d ", a[i]);
    
    printf("\n");
}
